using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowPool : MonoBehaviour
{
    public static ArrowPool main;

    [Header("Settings")]
    public GameObject ArrowPrefab;
    public int poolSize = 20;

    public List<Arrow> availableArrows;

    private void Awake()
    {
        main = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        availableArrows = new List<Arrow>();

        for (int i = 0; i < poolSize; i++)
        {
            Arrow arrow = Instantiate(ArrowPrefab, transform).GetComponent<Arrow>();
            arrow.gameObject.SetActive(false);

            availableArrows.Add(arrow);
        }
    }

    /// <summary>
    /// Spawn arrow
    /// Created by :NamVTP (20/6/2022)
    /// </summary>
    /// <param name="position"></param>
    /// <param name="Velocity"></param>
    public void PickFromPool(Vector3 position, Vector3 velocity, Quaternion rotation)
    {
        if (availableArrows.Count < 1)
            return;

        availableArrows[0].Activate(position, velocity, rotation, false);

        availableArrows.RemoveAt(0);
    }

    /// <summary>
    /// Add bullet back to pool
    /// Created by :NamVTP (26/5/2022)
    /// </summary>
    /// <param name="bullet"></param>
    public void AddToPool(Arrow arrow)
    {
        if (!availableArrows.Contains(arrow))
            availableArrows.Add(arrow);
    }
}
