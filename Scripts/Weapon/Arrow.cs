using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Arrow : MonoBehaviour
{
    public Rigidbody rigidBody;

    public float lifeTime;
    public int arrowDamage;

    EnemyStat target;
    Interactible explosive;

    private bool _isGhost;

    public void Activate(Vector3 position, Vector3 velocity, Quaternion rotation, bool isGhost)
    {
        _isGhost = isGhost;

        transform.position = position;
        rigidBody.velocity = velocity;
        transform.rotation = rotation;

        //rigidBody.AddForce(velocity, ForceMode.Impulse);

        gameObject.SetActive(true);

        StartCoroutine("Decay");
    }

    public void Deactivate()
    {
        ArrowPool.main.AddToPool(this);

        StopAllCoroutines();

        gameObject.SetActive(false);
        //Destroy(gameObject);
    }

    private IEnumerator Decay()
    {
        yield return new WaitForSeconds(lifeTime);

        Deactivate();
    }

    public void OnTriggerEnter(Collider other)
    {
        if (_isGhost)
            return;
        FindObjectOfType<AudioManager>().Play("hit");

        target = other.transform.GetComponent<EnemyStat>();
        if (target != null)
            target.TakeDamage(arrowDamage);

        Deactivate();
    }
}
