using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Weapon : MonoBehaviour
{
    [SerializeField] private ArrowPool arrowPool;
    public Transform fpCamera;
    public Transform firePoint;

    [Header("Gun setting")]
    [Range(0, 30)]
    public float arrowSpeed = 0;

    [Header("State")]
    public bool isFiring;
    public float fireSpeed;
    public float fireTimer;

    [Header("Animator")]
    public Animator animator;
    private string animatorParam = "Bow Power";
    public GameObject arrowDisplay;

    public Slider bowAmount;

    public int arrowConsumed;

    [SerializeField] private Projection _projection;



    private void Awake()
    {
        animator = GetComponent<Animator>();
        _projection = GetComponent<Projection>();
        
    }

    // Start is called before the first frame update
    void Start()
    {
        arrowPool = ArrowPool.main;
        arrowConsumed = 0;
    }

    void Update()
    {
        animator.SetFloat(animatorParam, bowAmount.value);

        if (bowAmount.value == bowAmount.maxValue)
            Lock();

        Vector3 arrowVelocity = fpCamera.forward * arrowSpeed;
        _projection.SimulateTrajectory(arrowPool.availableArrows[1], firePoint.position, arrowVelocity);
        
        if (isFiring)
        {
            if (fireTimer > 0)
                fireTimer -= Time.deltaTime;

            else
            {
                fireTimer = fireSpeed;
                Shoot();
            }
        }

        
    }

    /// <summary>
    /// Handle shooting
    /// Created by :NamVTP (26/5/2022)
    /// </summary>
    public void Shoot()
    {
        Vector3 arrowVelocity = fpCamera.forward * arrowSpeed;

        arrowPool.PickFromPool(firePoint.position, arrowVelocity, firePoint.rotation);

        FindObjectOfType<AudioManager>().Play("shoot");

        arrowConsumed++;
        
        //animator.SetTrigger("fire");
    }

    void Lock()
    {
        bowAmount.interactable = false;
    }

    public void UnlockAndReset()
    {
        bowAmount.value = 0;
        bowAmount.interactable = true;
    }
}
