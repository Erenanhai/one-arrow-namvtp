using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Projection : MonoBehaviour
{
    private Scene _simulationScene;
    private PhysicsScene _physicsScene;

    private Weapon weapon;

    [Header("Line")]
    [SerializeField] private LineRenderer _line;
    [SerializeField] private int _maxPhysicsFrameIterations = 100;

    private void Awake()
    {
        weapon = GetComponent<Weapon>();
        _line = GetComponent<LineRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
        CreatePhysicsScene();
    }

    void CreatePhysicsScene()
    {
        _simulationScene = SceneManager.CreateScene("Simulation", new CreateSceneParameters(LocalPhysicsMode.Physics3D));
        _physicsScene = _simulationScene.GetPhysicsScene();
    }

    public void SimulateTrajectory(Arrow arrow, Vector3 position, Vector3 velocity)
    {
        var ghostObject = Instantiate(arrow, position, Quaternion.identity);
        //ghostObject.GetComponent<Renderer>().enabled = false;
        SceneManager.MoveGameObjectToScene(ghostObject.gameObject, _simulationScene);

        ghostObject.Activate(position, velocity, weapon.firePoint.rotation, true);

        _line.positionCount = _maxPhysicsFrameIterations;

        for(int i = 0; i < _maxPhysicsFrameIterations; i++)
        {
            _physicsScene.Simulate(Time.fixedDeltaTime);
            _line.SetPosition(i, ghostObject.transform.position);
        }

        Destroy(ghostObject.gameObject);
    }
}
