using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Audio;

public class GameMenu : MonoBehaviour
{
    AudioManager audioManager;

    [SerializeField] private Weapon weapon;
    [SerializeField] private Text arrowConsumedDisplay;

    [Header("Level Display")]
    public Text levelDisplay;

    [Header("Pause Screen")]
    public GameObject pauseMenu;
    public static bool isPause = false;

    [Header("Loading Screen")]
    public GameObject loadingScreen;
    public Slider loadingSlider;
    public Text progressText;

    [Header("Win Screen")]
    public GameObject winScreen;
    public GameObject[] stars;
    public const int threeStarCondition = 2;
    public const int twoStarCondition = 3;

    [Header("Lose Screen")]
    public GameObject loseScreen;

    [HideInInspector]
    public Animator animator;

    public int levelNumber;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    private void Start()
    {
        if (SceneManager.GetActiveScene().name == "Loading")
        {
            LoadLevel(1);
        }
        else
            Pause();
    }

    private void Update()
    {
        arrowConsumedDisplay.text = "Used arrows: " + weapon.arrowConsumed;

        levelDisplay.text = SceneManager.GetActiveScene().name;
    }

    /// <summary>
    /// Load level sceme
    /// </summary>
    /// <param name="level"></param>
    public void LoadLevel(int level)
    {
        StartCoroutine(LoadAsynchronously(level));
    }

    /// <summary>
    /// Load level while displaying loading screen
    /// </summary>
    /// <param name="levelIndex"></param>
    /// <returns></returns>
    IEnumerator LoadAsynchronously (int levelIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(levelIndex);

        loadingScreen.SetActive(true);

        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);

            //Debug.Log(progress);
            loadingSlider.value = progress;
            progressText.text = "Loading... " + progress * 100 + "%";

            yield return null;
        }
    }

    /// <summary>
    /// Display pause menu
    /// </summary>
    public void Pause()
    {
        pauseMenu.SetActive(true);
        Time.timeScale = 0;
        audioManager.PauseAll();
        isPause = true;
    }

    /// <summary>
    /// Continue the game
    /// </summary>
    public void Resume()
    {
        pauseMenu.SetActive(false);
        Time.timeScale = 1;
        audioManager.Continue();
        isPause = false;
    }

    /// <summary>
    /// Go back to main menu
    /// </summary>
    public void BackToMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("0_Main Menu");
    }

    /// <summary>
    /// Restart level
    /// </summary>
    public void Restart()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    /// <summary>
    /// Display win screen
    /// </summary>
    public void Win()
    {
        winScreen.SetActive(true);

        switch (weapon.arrowConsumed)
        {
            case threeStarCondition:
                DisplayStars(3);
                break;
            case twoStarCondition:
                DisplayStars(2);
                break;
        }

        if (weapon.arrowConsumed > twoStarCondition)
            DisplayStars(1);

        Time.timeScale = 0;
    }

    /// <summary>
    /// Calculate points (stars) according to arrow consumed.
    /// </summary>
    /// <param name="starNumber"></param>
    private void DisplayStars(int starNumber)
    {
        for (int i = 0; i < starNumber; i++)
        {
            stars[i].SetActive(true);
        }
    }

    /// <summary>
    /// Display lose screen
    /// </summary>
    public void Lose()
    {
        Time.timeScale = 0;
        loseScreen.SetActive(true);
    }

    /// <summary>
    /// Trigger damage screen effect for player
    /// </summary>
    public void Damage()
    {
        animator.SetTrigger("damage");
    }

    public void Quit()
    {
        Application.Quit();
    }
}
