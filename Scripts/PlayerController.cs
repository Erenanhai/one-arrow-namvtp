using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public PlayerController playerController;

    //[Header("Touch Detection")]
    //int upperFingerId, lowerFingerId;
    //float lookScreenWidth;

    [Header("First person camera")]
    public Transform fpCameraTransform;

    [Header("Camera Control")]
    Vector2 lookInput;
    float cameraPitch;
    public float cameraSensitivity;

    Weapon weapon;

    private void Awake()
    {
        playerController = GetComponent<PlayerController>();
        fpCameraTransform = GetComponentInChildren<Camera>(gameObject).transform;
        weapon = GetComponentInChildren<Weapon>();
    }

    // Start is called before the first frame update
    void Start()
    {
        //touch is not being tracked (initialize)
        //upperFingerId = -1;
        ////lowerFingerId = -1;

        //lookScreenWidth = Screen.height / 3;
    }

    // Update is called once per frame
    void Update()
    {
        GetTouchInput();

        //if (upperFingerId != -1)
        //{
            LookAround();
        //}

        //if (lowerFingerId != -1)
        //{
        //    Action();
        //}
    }

    public void GetTouchInput()
    {
        //for (int i = 0; i < Input.touchCount; i++)
        //{
            Touch touch = Input.GetTouch(0);

            switch (touch.phase)
            {
                //case TouchPhase.Began:
                //    if (touch.position.x < lookScreenWidth && lowerFingerId == -1)
                //    {
                //        lowerFingerId = touch.fingerId;
                //        
                //    }
                //    else if (touch.position.x > lookScreenWidth && upperFingerId == -1)
                //    {
                //        upperFingerId = touch.fingerId;
                //        
                //   }
                //    break;

                case TouchPhase.Ended:
                    //Debug.Log("Stop tracking");
                    if (weapon.bowAmount.value == weapon.bowAmount.maxValue)
                    {
                        StartCoroutine(Fire());   
                    }
                    weapon.UnlockAndReset();

                    break;
                //    if (touch.fingerId == lowerFingerId)
                //    {
                //        lowerFingerId = -1;
                        
                //    }
                //    else if (touch.fingerId == upperFingerId)
                //    {
                //        upperFingerId = -1;
                        
                //    }
                //    break;

                case TouchPhase.Moved:
                    // Get input for looking around
                    lookInput = touch.deltaPosition * cameraSensitivity * Time.deltaTime;
                    break;

                case TouchPhase.Stationary:
                    // Set input to stop looking around (zero)
                    lookInput = Vector2.zero;
                    break;
            //}
        }
    }
    /// <summary>
    /// Handle looking
    /// Created by :NamVTP (18/6/2022)
    /// </summary>
    /// <returns></returns>
    private void LookAround()
    {
        // Vertical rotation
        cameraPitch = Mathf.Clamp(cameraPitch - lookInput.y, -90f, 90f);
        fpCameraTransform.localRotation = Quaternion.Euler(cameraPitch, 0, 0);
         
        // Horizontal rotation
        transform.Rotate(transform.up, lookInput.x);
    }

    private IEnumerator Fire()
    {
        weapon.arrowDisplay.SetActive(false);
        weapon.animator.SetTrigger("Fire Bow");
        yield return new WaitForSeconds(0.5f);
        weapon.arrowDisplay.SetActive(true);
        //weapon.UnlockAndReset();
    }
}
