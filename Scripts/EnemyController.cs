using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    NavMeshAgent agent;

    public EnemyStat enemyStat;

    public Transform deadEnemy;

    public LayerMask isGround;
    public LayerMask enemyMask;

    [Header("Patroling")]
    public Vector3 walkPoint;
    bool walkPointSet;
    public float walkPointRange;

    [Header("Spot died enemy")]
    public bool deadEnemyInSight;
    public bool deadEnemySpoted;
    public bool alarmed;

    [Header("States")]
    public float sightRange;
    public float spotRange;

    private void Update()
    {
        
    }
}
