using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CharacterAnimator : MonoBehaviour
{
    NavMeshAgent agent;
    protected Animator animator;
    //protected Combat combat;
    public AnimatorOverrideController overrideController;

    public AnimationClip[] defaultAttackAnimationSet;
    protected AnimationClip[] currentAttackAnimationSet;
    public AnimationClip replaceableAttackAnimation;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponentInChildren<Animator>();
        //combat = GetComponent<Combat>();

        if (overrideController == null)
            overrideController = new AnimatorOverrideController(animator.runtimeAnimatorController);
        
        animator.runtimeAnimatorController = overrideController;

        currentAttackAnimationSet = defaultAttackAnimationSet;
        //combat.OnAttack += OnAttack;
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        float speedPercent = agent.velocity.magnitude / agent.speed;
        animator.SetFloat("speed", speedPercent, .1f, Time.deltaTime);

        //animator.SetBool("inCombat", combat.InCombat);
    }
}
