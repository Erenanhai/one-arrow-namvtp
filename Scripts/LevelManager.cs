using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    GameMenu menu;
    public GameObject[] enemyDied;
    [SerializeField] private int enemyCount;

    private void Awake()
    {
        menu = FindObjectOfType<GameMenu>();
        //DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        enemyDied = null;
        enemyCount = GameObject.FindGameObjectsWithTag("Enemy").Length;
    }

    // Update is called once per frame
    void Update()
    {
        enemyDied = GameObject.FindGameObjectsWithTag("Dead");

        if(enemyCount == enemyDied.Length)
        {
            menu.Win();
        }
    }
}
