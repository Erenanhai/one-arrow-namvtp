using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStat : MonoBehaviour
{
    [Header("Health")]
    public float maxHealth;
    public float currentHealth;

    GameMenu menu;
    Animator animator;
    //public GameManager gameManager;
    public bool isDead;
    //[SerializeField] private GameMenu menu;
    int deadEnemyLayer;
    public bool alarmed;

    private void Awake()
    {
        menu = FindObjectOfType<GameMenu>();
        animator = GetComponentInChildren<Animator>();
        //gameManager = GameManager.instance;
        isDead = false;
        alarmed = false;

        deadEnemyLayer = LayerMask.NameToLayer("Dead Enemy");
    }

    private void Start()
    {
        currentHealth = maxHealth;
    }

    private void Update()
    {
        if (alarmed)
            menu.Lose();
    }

    /// <summary>
    /// Handle damage
    /// </summary>
    /// <param name="damage"></param>
    public void TakeDamage(int damage)
    {
        if (!isDead)
        {
            currentHealth -= damage;

            if (currentHealth <= 0)
            {
                currentHealth = 0;
                Die();
            }
        }
        else
            return;
    }

    /// <summary>
    /// Handle dead
    /// </summary>
    public void Die()
    {
        isDead = true;
        animator.Play("dead");
        //Destroy(gameObject, 1f);

        gameObject.layer = deadEnemyLayer;
        gameObject.tag = "Dead";

        return;
    }
}
