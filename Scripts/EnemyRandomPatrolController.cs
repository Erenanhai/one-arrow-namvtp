using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyRandomPatrolController : MonoBehaviour
{
    NavMeshAgent agent;

    public EnemyStat enemyStat;

    public Transform deadEnemy;

    public LayerMask isGround;
    public LayerMask deadEnemyMask;

    [Header("Patroling")]
    public Vector3 walkPoint;
    bool walkPointSet;
    public float walkPointRange;
    [SerializeField] private bool walkHorizontally;
    [SerializeField] private bool walkVertically;

    [Header("Spot died enemy")]
    public bool deadEnemyInSight;
    public bool deadEnemySpoted;
    //public bool alarmed;

    [Header("States")]
    public float sightRange;
    public float spotRange;

    private bool isCoroutineStarted = false;


    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        enemyStat = GetComponent<EnemyStat>();
        enemyStat.alarmed = false;
    }

    private void Start()
    {
        InvokeRepeating("Patroling", 1f, 1f);
    }

    private void Update()
    {
        if (!enemyStat.isDead)
        {
            deadEnemyInSight = Physics.CheckSphere(transform.position, sightRange, deadEnemyMask);
            deadEnemySpoted = Physics.CheckSphere(transform.position, spotRange, deadEnemyMask);

            //if (!deadEnemyInSight && !deadEnemySpoted)
            //    Patroling();
            if (deadEnemyInSight && !deadEnemySpoted)
            {
                StopAllCoroutines();
                Suspicious();
            }
            else if (deadEnemyInSight && deadEnemySpoted)
            {
                StopAllCoroutines();
                
                agent.SetDestination(transform.position);
                transform.LookAt(deadEnemy);
                
                Invoke("Alarm", 3f);
            }
        }
        else
        {
            StopAllCoroutines();
            agent.SetDestination(transform.position);
            return;
        }
    }

    private void SearchWalkPoint()
    {
            float randomZ = Random.Range(-walkPointRange, walkPointRange);
            float randomX = Random.Range(-walkPointRange, walkPointRange);

            if (walkHorizontally)
                randomZ = 0;
            if (walkVertically)
                randomX = 0;

            walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);

            if (Physics.Raycast(walkPoint, -transform.up, 2f, isGround))
            {
                walkPointSet = true;
            }
        
    }

    //private IEnumerator SetWalkPoint()
    //{
    //    isCoroutineStarted = true;
    //    yield return new WaitForSeconds(3f);
    //    SearchWalkPoint();
    //}

    private void Patroling()
    {

        if (!enemyStat.isDead)
        {
            if (deadEnemyInSight || deadEnemySpoted)
                return;
            if (!walkPointSet)
            {
                //if (!isCoroutineStarted)
                //    StartCoroutine(SetWalkPoint());
                SearchWalkPoint();
            }
            else
                agent.SetDestination(walkPoint);

            Vector3 distanceToWalkPoint = transform.position - walkPoint;

            if (distanceToWalkPoint.magnitude < 1f)
            {
                walkPointSet = false;
            }
        }
        else
            return;
    }

    private void Suspicious()
    {
        deadEnemy = GameObject.FindGameObjectWithTag("Dead").transform;
        agent.SetDestination(deadEnemy.position);
    }

    private void Alarm()
    {
        enemyStat.alarmed = true;
    }
}
